@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit User Profile</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Update Your Profile</strong></div>
              <div class="block-body">
                @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif
                <form class="form-horizontal" action="/admin/freelancer/update_profile" method="post" enctype="multipart/form-data">
                @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">First Name</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" disabled="" name="first_name" placeholder="Suresh" value="{{$Users->first_name}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Last Name</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" disabled="" name="last_name" placeholder="Vankar" value="{{$Users->last_name}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Email Address</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" disabled="" placeholder="info@techitalents.com" name="email" value="{{$Users->email}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Gender</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" placeholder="" value="{{$Users->gender}}" name="gender">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Upload Profile Image</label>
                    <div class="col-sm-6">
                      <input type="file" class="form-control" id="ChangeImg" name="profile_pic" value="">
                    </div>

                    <div class="col-sm-3">
                      <h4>Preview</h4>
                      <img src="/Admin/freelancer/profile_pic/{{$Users->profile_pic}}" id="img_preview" alt="" style="
                      width: 300px;
                  ">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Contact number</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="contact_no" value="{{$Users->contact_no}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Country</label>
                    <div class="col-sm-6">
                      <input type="texy" class="form-control" name="country" value="{{$Users->country}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">State</label>
                    <div class="col-sm-6">
                      <input type="text"  class="form-control" name="state" value="{{$Users->state}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">City</label>
                    <div class="col-sm-6">
                      <input type="text"  class="form-control" name="city" value="{{$Users->city}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Pincode</label>
                    <div class="col-sm-6">
                      <input type="text"  class="form-control" name="pincode" value="{{$Users->pincode}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offers</label>
                    <div class="col-sm-6">
                      <input type="text"  class="form-control" name="offers" value="{{$Users->offers}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Area</label>
                    <div class="col-sm-6">
                      <input type="text"  class="form-control" name="area" value="{{$Users->area}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Qualification</label>
                    <div class="col-sm-6">
                      <input type="text" value="{{$Users->qualification}}" name="qualification" class="form-control">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Experience</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="experience_years" value="{{$Users->experience_years}}">
                          <div class="input-group-append"><span class="input-group-text">Yrs</span></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                        <div class="input-group">
                          <input type="text" class="form-control" name="experience_months" value="{{$Users->experience_months}}">
                          <div class="input-group-append"><span class="input-group-text">Mos</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Skills</label>
                    <div class="col-sm-6">
                      {{-- <select multiple="" class="form-control" name="skills">
                        <option>option 1</option>
                        <option>option 2</option>
                        <option>option 3</option>
                        <option>option 4</option>
                      </select> --}}
                      <input type="text" class="form-control" name="skills" value="{{$Users->skills}}" data-role="tagsinput">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <div class="col-sm-9 ml-auto">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
         
           <p class="no-margin-bottom">2020 &copy; Taprecruiter. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection