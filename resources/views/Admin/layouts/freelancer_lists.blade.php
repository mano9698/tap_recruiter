@extends('Admin.base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Freelancer list</h2>
      </div>
    </div>
    
    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item active">Freelancer List            </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              
              <div class="table-responsive"> 
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Name & Lastname</th>
                      <th>Email Id</th>
                      <th>Contact Number</th>
                      <th>Country/State/City</th>
                      <th>Area - Pincode</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Users)
                      @foreach($Users as $User)
                    <tr>
                      <th scope="row">{{$User->id}}</th>
                      <!--<td><div class="avatar"> <img src="../img/avatar-1.jpg" alt="..." class="img-fluid"></div><a href="#" class="name"></td>-->
                      <td><strong class="d-block">{{$User->first_name}}</strong><span class="d-block">{{$User->last_name}}</span></a></td>
                      <td>{{$User->email}}</td>
                      <td>{{$User->contact_no}}</td>
                      <td>{{$User->country}}/{{$User->state}}/{{$User->city}}</td>
                      <td>{{$User->area}} - {{$User->pincode}}</td>
                      <td>
                        <input data-id="{{$User->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $User->status ? 'checked' : '' }}>
                     </td>
                      <td>
                        <a href="/admin/edit_freelancer/{{$User->id}}" class="btn button-sm blue">Edit</a>
                     </td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
          
          
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Taprecruiter. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection