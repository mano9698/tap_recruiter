
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css" />

	<!--<link rel="stylesheet" href="one-page/css/et-line.css" type="text/css" />-->

	<link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!--<link rel="stylesheet" href="{{URL::asset('UI/css/colors.php?color=fcc741')}}" type="text/css" />-->

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="{{URL::asset('UI/course/css/fonts.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/course/course.css')}}" type="text/css" />

	<!-- Document Title
	============================================= -->
	<title>::Welcome to Cacpaworld::</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		

		<!-- Top Bar
		============================================= -->
		<div id="top-bar">

			<div class="container clearfix">

				<div class="col_half nobottommargin clearfix">

					<!-- Top Links
					============================================= -->
					<div class="top-links">
						{{-- <ul>
							<li><a href="/">Home</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Contact</a></li>
						</ul> --}}
					</div><!-- .top-links end -->

				</div>

				<div class="col_half fright col_last clearfix nobottommargin">

					<!-- Top Social
					============================================= -->
					<div id="top-social">
						{{-- <ul>
							<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
							<li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
							<li><a href="tel:+91.9916669702" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+91.9916669702</span></a></li>
							<li><a href="/cdn-cgi/l/email-protection#6b02050d042b1f0e0803021f0a070e051f1845080406" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text"><span class="__cf_email__" data-cfemail="c0a9aea6af80b4a5a3a8a9b4a1aca5aeb4b3eea3afad">[email&#160;protected]</span></span></a></li>
						</ul> --}}
					</div><!-- #top-social end -->

				</div>

			</div>

		</div><!-- #top-bar end -->

		<!-- Header
		============================================= -->
<!-- #header end -->
		
		<!-- Content
		============================================= -->
		<!-- Page Title
		============================================= 
		<section id="page-title">

			<div class="container clearfix">
				<h1>My Account</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">Login</li>
				</ol>
			</div>

		</section>--><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="col_one_third nobottommargin">

						
            <div class="login_left">
                <div class="login_left_img"><img src="{{URL::asset('UI/images/login-bg.jpg')}}" alt="login background" style="height: 400px;"></div>
            </div>
        

					</div>

					<div class="col_two_third col_last nobottommargin">


						<h3>Hello, Welcome Back.</h3>

<!--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde, vel odio non dicta provident sint ex autem mollitia dolorem illum repellat ipsum aliquid illo similique sapiente fugiat minus ratione.</p>-->

                        <form id="login-form" name="login-form" class="nobottommargin" action="/users/admin_login" method="post">
                            @csrf
									<h4>Login to your Account</h4>

									<div class="col_half">
										<label for="login-form-username">Username:</label>
										<input type="text" id="login-form-username" name="email" value="" class="form-control not-dark" />
									</div>
                                    <br>
                                    <br>
									<br>
									<br>
									<br>
									<div class="col_half">
										<label for="login-form-password">Password:</label>
										<input type="password" id="login-form-password" name="password" value="" class="form-control not-dark" />
									</div>
                                    <br>
                                    <br>
									<br>
									<br>
									<br>
									<div class="col_half nobottommargin">
										<button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login" type="submit">Login</button>
										{{-- <a href="#" class="fright">Forgot Password?</a> --}}
									</div>
								</form>

					</div>

				</div>

			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="row align-items-center justify-content-between">
						<div class="col-md-5">
							Designed &amp; Developed By <a href="http://www.techitalents.com">Techitalents</a> 
							
						</div>

						<div class="col-md-7 tright">
							&copy; Copyrights 2020&nbsp;<a href="#">Techitalents.com</a>&nbsp;All rights reserved.
						</div>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
    {{-- <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">
    </script> --}}
    <script src="{{URL::asset('UI/js/jquery.js')}}"></script>
	<script src="{{URL::asset('UI/js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{URL::asset('UI/js/functions.js')}}"></script>

</body>
</html>