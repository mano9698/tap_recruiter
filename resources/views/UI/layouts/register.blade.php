<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Stylesheets
	============================================= -->
	<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css" />

	<!--<link rel="stylesheet" href="one-page/css/et-line.css" type="text/css" />-->

	<link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!--<link rel="stylesheet" href="{{URL::asset('UI/css/colors.php?color=fcc741')}}" type="text/css" />-->

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="{{URL::asset('UI/course/css/fonts.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/course/course.css')}}" type="text/css" />

	<!-- Document Title
	============================================= -->
	<title>{{$title}}</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">


		<!-- Top Bar
		============================================= -->
		<div id="top-bar">

			<div class="container clearfix">

				<div class="col_half nobottommargin clearfix">

					<!-- Top Links
					============================================= -->
					{{-- <div class="top-links">
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</div> --}}
					<!-- .top-links end -->

				</div>

				<div class="col_half fright col_last clearfix nobottommargin">

					<!-- Top Social
					============================================= -->
					{{-- <div id="top-social">
						<ul>
							<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
							<li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
							<li><a href="tel:+91.9916669702" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+91.9916669702</span></a></li>
							<li><a href="mailto:info@techitalents.com" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">info@techitalents.com</span></a></li>
						</ul>
					</div> --}}
					<!-- #top-social end -->

				</div>

			</div>

		</div><!-- #top-bar end -->

		<!-- Header
		============================================= -->
<!-- #header end -->

		<!-- Content
		============================================= -->
		<!-- Page Title
		=============================================
		<section id="page-title">

			<div class="container clearfix">
				<h1>My Account</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">Login</li>
				</ol>
			</div>

		</section>--><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="col_one_third nobottommargin">


            <div class="login_left">
                <div class="login_left_img"><img src="{{URL::asset('UI/images/login-bg.jpg')}}" alt="login background" style="height: 400px;"></div>
            </div>


					</div>

					<div class="col_two_third col_last nobottommargin">


						<h3>Don't have an Account? Register Now.</h3>

<!--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde, vel odio non dicta provident sint ex autem mollitia dolorem illum repellat ipsum aliquid illo similique sapiente fugiat minus ratione.</p>-->
							@if(session('message'))
								<div class="alert alert-success width100">
									<ul>
										<li>{!! session('message') !!}</li>
									</ul>
								</div>
							@endif

						<form id="location" name="register-form" class="nobottommargin" action="/users/store_users" method="post">
								@csrf
								<div class="col_half">
								<label for="register-form-fname">First Name:</label>
								<input type="text" id="register-form-fname" name="first_name" value="" class="form-control" />
							</div>

							<div class="col_half col_last">
								<label for="register-form-lname">Last Name:</label>
								<input type="text" id="register-form-lname" name="last_name" value="" class="form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_half">
								<label for="register-form-email">Email ID:</label>
								<input type="text" id="register-form-email" name="email" value="" class="form-control" />
                            </div>

                            <div class="col_half col_last">
								<label for="register-form-email">Password:</label>
								<input type="password" id="register-form-email" name="password" value="" class="form-control" />
							</div>



							<div class="clear"></div>

							<div class="col_half ">
								<label for="register-form-phone">Contact Number:</label>
								<input type="text" id="register-form-phone" name="contact_no" value="" class="form-control" />
							</div>

							<div class="col_half col_last">
								<label for="register-form-country">Country:</label>

								<input type="text" id="countryId" name="country" value="" class="form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_half ">
								<label for="register-form-state">State:</label>

								<input type="text" id="stateId" name="state" value="" class="form-control" />
							</div>

							<div class="col_half col_last">
									<label for="register-form-city">City:</label>

								<input type="text" id="cityId" name="city" value="" class="form-control" />
							</div>





							<div class="clear"></div>
                            <div class="col_half">

								<label for="register-form-area">Area:</label>
								<input type="text" id="register-form-area" name="area" value="" class="form-control" />
							</div>

                            <div class="col_half col_last">
								<label for="register-form-pincode">Pincode:</label>
								<input type="text" id="register-form-pincode" name="pincode" value="" class="form-control" />
							</div>
							<div class="clear"></div>


							<div class="col_half ">
								<label for="register-form-client">Select Gender</label>
								<select  id="register-form-client" name="gender" value="" class="form-control" />
									<option>Male</option>
									<option>Female</option>
								</select>
							</div>

							<div class="col_half col_last">
								<label for="register-form-pincode">Category:</label>
								@if(request()->segment(1) == "users")
									<input type="text" id="register-form-pincode" name="" value="Taprecruiter" class="form-control" disabled/>
                                @elseif(Request::segment(1) == "accountswale")
									<input type="text" id="register-form-pincode" name="" value="Accountswale" class="form-control" disabled/>
								@elseif(Request::segment(1) == "cacpa")
									<input type="text" id="register-form-pincode" name="" value="Cacpa" class="form-control" disabled/>
								@elseif(Request::segment(1) == "Taprecruiter")
									<input type="text" id="register-form-pincode" name="" value="Taprecruiter" class="form-control" disabled/>
								@elseif(Request::segment(1) == "tuition_booking")
									<input type="text" id="register-form-pincode" name="" value="Tuition Booking" class="form-control" disabled/>
								@endif
							</div>

							<div class="col_half col_last" style="display: none;">
								<label for="register-form-pincode">Category:</label>
								@if(request()->segment(1) == "users")
									<input type="text" id="register-form-pincode" name="user_type" value="2" class="form-control"/>

								@elseif(Request::segment(1) == "cacpa")
									<input type="text" id="register-form-pincode" name="user_type" value="3" class="form-control" />
								@elseif(Request::segment(1) == "Taprecruiter")
									<input type="text" id="register-form-pincode" name="user_type" value="4" class="form-control" />
								@elseif(Request::segment(1) == "tuition_booking")
									<input type="text" id="register-form-pincode" name="user_type" value="5" class="form-control"/>
								@endif
							</div>
							<div class="clear"></div>
							<div class="clear"></div>

							<div class="col_full nobottommargin">
								<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register" type="submit">Register Now</button>
							</div>
							</form>

					</div>

				</div>

			</div>

		</section><!-- #content end -->
        <br>
        <br>
        <br>
		<!-- Footer
		============================================= -->
        @include('UI.common.footer')

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{URL::asset('UI/js/jquery.js')}}"></script>
	<script src="{{URL::asset('UI/js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{URL::asset('UI/js/functions.js')}}"></script>

	<script src="{{URL::asset('UI/js/custom/locations.js')}}"></script>

</body>
</html>
