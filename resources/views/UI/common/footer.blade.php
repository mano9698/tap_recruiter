<footer id="footer" class="dark">

    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        {{-- <div class="footer-widgets-wrap clearfix">

            

            Looking to hire for long-term or full-time recruiters? See how Taprecruiter simplifies.

        </div> --}}
        <!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half">
                Copyrights &copy; 2020 Taprecruiter - All Rights Reserved.<br>
                {{-- <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> --}}
            </div>

            <div class="col_half col_last tright">
                {{-- <div class="fright clearfix">
                    <a href="#" class="social-icon si-small si-borderless si-facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="social-icon si-small si-borderless si-twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="#" class="social-icon si-small si-borderless si-gplus">
                        <i class="icon-gplus"></i>
                        <i class="icon-gplus"></i>
                    </a>
                    <a href="#" class="social-icon si-small si-borderless si-linkedin">
                        <i class="icon-linkedin"></i>
                        <i class="icon-linkedin"></i>
                    </a>
                </div> --}}

                <div class="clear"></div>

                <i class="icon-envelope2"></i> info@taprecruiter.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +91-99166 69702
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->