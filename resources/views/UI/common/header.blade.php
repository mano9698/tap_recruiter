<header id="header" class="transparent-header page-section dark">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="#" class="standard-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark.png')}}"><img src="{{URL::asset('UI/images/logo.png')}}" alt="Canvas Logo"></a>
                <a href="#" class="retina-logo" data-dark-logo="{{URL::asset('UI/images/logo-dark@2x.png')}}"><img src="{{URL::asset('UI/images/logo@2x.png')}}" alt="Canvas Logo"></a>
            </div><!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">
						
                <ul class="one-page-menu">
                    {{-- <li class="menu_phone">86184 14801<br><span>9am - 6pm, Mon to Sat </span></li> --}}
                    <li><a href="/users/register"><div>Register</div></a></li>
                    <li><a href="/users/login"><div>Login</div></a></li>
                </ul>

            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header><!-- #header end -->
