@extends('UI.base')
@section('Content')

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">
            <form action="/freelancer/search" method="post" class="landing-wide-form clearfix" style="width:100%;">
                @csrf
                <div class="col_four_fifth nobottommargin">
                    <div class="col_one_fourth nobottommargin">
                        <input type="text" class="form-control form-control-lg not-dark" name="country" value="{{Session::get('Country')}}" placeholder="Country*">
                    </div>
                    <div class="col_one_fifth nobottommargin">
                        <input type="text" class="form-control form-control-lg not-dark" name="state" value="{{Session::get('State')}}" placeholder="State*">
                    </div>
                    <div class="col_one_fifth nobottommargin">
                        <input type="text" class="form-control form-control-lg not-dark" name="city" value="{{Session::get('City')}}" placeholder="City*">
                    </div>
                    <div class="col_one_fifth nobottommargin">
                        <input type="text" class="form-control form-control-lg not-dark" value="{{Session::get('Area')}}" placeholder="Area*" name="area">
                    </div>
                    <div class="col_one_fifth nobottommargin">
                        <select class="form-control form-control-lg not-dark" id="exampleFormControlSelect1" value="" name="gender">
                                <option selected disabled>Gender</option>
                                <option value="Male" @if(Session::get('Gender') == "Male") selected @endif>Male</option>
                                <option value="Female" @if(Session::get('Gender') == "Female") selected @endif>Female</option>
                            </select>

                    </div>
                </div>
                <div class="col_one_fifth col_last nobottommargin">
                    <button class="btn btn-lg btn-danger btn-block nomargin" value="submit" type="submit" style="">Search Freelancer</button>
                </div>
            </form>

            <div class="clear"></div>

        </div>

        <div class="container">
            <div class="row justify-content-center d-flex">
                <div class="col-lg-12 post-list">

                    @foreach($Users as $User)
                        <div class="single-post d-flex flex-row">
                            <div class="thumb col-lg-2">
                                <img src="/Admin/freelancer/profile_pic/{{$User->profile_pic}}" alt="">

                            </div>
                            <div class="details col-lg-3">
                                <div class="title d-flex flex-row justify-content-between">
                                    <div class="titles">
                                        <a href="#">
                                            <h4 class="mb-1">{{$User->first_name}} {{$User->last_name}}, {{$User->gender}}</h4>
                                        </a>
                                        <h6>{{$User->city}},{{$User->state}},{{$User->country}}</h6>
                                    </div>

                                </div>
                                <h5>Qualifications: {{$User->qualification}}</h5>
                                <h5>Experience: {{$User->experience_years}} years, {{$User->experience_months}} months</h5>
                            </div>
                            <div class="thumb col-lg-4">
                                <h5>Skills</h5>
                                <ul class="tags my-0">
                                    <?php 
                                        $Skills = explode(',',$User->skills);
                                        // print_r($Skills);
                                        // exit;

                                        // echo $Skills[0];
                                        // exit;
                                    ?>
                                    @for($i=0; $i<count($Skills); $i++)
                                        <li>
                                            <a href="#">{{$Skills[$i]}}</a>
                                        </li>
                                    @endfor
                                    
                                    {{-- <li>
                                        <a href="#">Accounting Software</a>
                                    </li>
                                    <li>
                                        <a href="#">API Development</a>
                                    </li>
                                    <li>
                                        <a href="#">Cloud Computing</a>
                                    </li>
                                    <li>
                                        <a href="#">Component Development</a>
                                    </li> --}}
                                </ul>
                                <h5>{{$User->offers}}</h5>
                            </div>
                            <div class="col-lg-3">
                                <ul class="btns">
                                <li><a href="/freelancer/request_call_back/{{$User->id}}" target="_blank">Request Call Back</a></li>
                                    <li style="background: #37bfa7;"><a href="/freelancer/get_quote/{{$User->id}}" target="_blank">Get a Quote</a></li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>

    </div>

</section>
@endsection