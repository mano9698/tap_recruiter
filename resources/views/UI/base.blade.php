<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="Tap Recruiter">
    <meta name="keywords" content="Freelance recruiter in india, Recruitment vendor near me, Payroll consultant, Recruitment consultant, Recruitment networking, Best Contract staffing company in India, Best contract staffing company in Bangalore, Best recruitment vendor in Bangalore, Recruitment Jobs in Bangalore, Recruiter  Jobs in India">
    <meta name="author" content="Tap Recruiter">


    <meta name="google-site-verification" content="RJ4gQVLM1AfGYhQvyATyiZ-z0unM2Gtc_raASFBblQw" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:200,300,400,500,600,700|Ubuntu:200,300,400,500,600,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{URL::asset('UI/css/responsive.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>{{$title}}</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		@include('UI.common.header')

		<section id="slider" class="slider-element slider-parallax" style="background: #000f60; background-size: cover" data-height-xl="100" data-height-lg="100" data-height-md="0" data-height-sm="0" data-height-xs="0">



		</section>



		<!-- Content
		============================================= -->
		@yield('Content')

		<!-- Footer
		============================================= -->
		@include('UI.common.footer')

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{URL::asset('UI/js/jquery.js')}}"></script>
	<script src="{{URL::asset('UI/js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{URL::asset('UI/js/functions.js')}}"></script>


</body>
</html>
