<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/', 'UI\HomeController@home');
Route::get('/', 'UI\FreelancerController@freelancer_lists');

Route::get('/{name}/register', 'UI\AuthendicationController@register');

Route::group(['prefix' => '/users'], function () {
    // Route::get('/register', 'UI\AuthendicationController@register');

    Route::get('/login', 'UI\AuthendicationController@login');

    Route::post('/store_users', 'UI\AuthendicationController@store_users');

    Route::post('/admin_login', 'UI\AuthendicationController@admin_login');
});


// Freelance

Route::group(['prefix' => '/freelancer'], function () {

    Route::post('/search', 'UI\FreelancerController@search_freelancer');

    Route::get('/request_call_back/{id}', 'UI\FreelancerController@request_call_back');

    Route::post('/store_request_call_back', 'UI\FreelancerController@store_request_call_back');

    Route::get('/get_quote/{id}', 'UI\FreelancerController@get_quote');

    Route::post('/store_get_quote', 'UI\FreelancerController@store_get_quote');
});
















// Admin
Route::group(['prefix' => '/admin/freelancer'], function () {

    Route::get('/dashboard', 'Admin\Freelancer\HomeController@dashboard');

    Route::get('/request_call_back_lists', 'Admin\Freelancer\FreelancerController@request_call_back_lists');

    Route::get('/get_quote_lists', 'Admin\Freelancer\FreelancerController@get_quote_lists');

    Route::get('/edit_profile', 'Admin\Freelancer\FreelancerController@edit_profile');

    Route::post('/update_profile', 'Admin\Freelancer\FreelancerController@update_profile');

    Route::get('/change_password', 'Admin\Freelancer\FreelancerController@change_password');

    Route::post('/update_password', 'Admin\Freelancer\FreelancerController@update_password');

    Route::get('/user_logout', 'Admin\Freelancer\FreelancerController@user_logout');
});



Route::group(['prefix' => '/admin'], function () {

    Route::get('/freelancer_list', 'Admin\HomeController@freelancer_list');

    Route::get('/dashboard', 'Admin\HomeController@dashboard');

    Route::get('/request_call_back_lists', 'Admin\HomeController@request_call_back_lists');

    Route::get('/get_quote_lists', 'Admin\HomeController@get_quote_lists');

    Route::get('/admin_logout', 'Admin\HomeController@admin_logout');

    Route::get('/add_freelancer', 'Admin\HomeController@add_freelancer');

    Route::get('/edit_freelancer/{id}', 'Admin\HomeController@edit_freelancer');

    Route::post('/store_freelancer', 'Admin\HomeController@store_freelancer');

    Route::post('/update_freelancer', 'Admin\HomeController@update_freelancer');

    Route::post('/changeStatus', 'Admin\HomeController@changeStatus');

    Route::get('/change_password', 'Admin\HomeController@change_password');

    Route::post('/update_password', 'Admin\HomeController@update_password');

    Route::get('/login', 'Admin\HomeController@login');
});
