<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestCallBack extends Model
{
    use HasFactory;

    protected $table = 'request_call_back';

    protected $fillable = ['user_id','name', 'email', 'contact', 'country', 'state', 'city', 'requirements'];
}
