<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;

class FreelancerController extends Controller
{
    public function freelancer_lists($id){
        $title ="::Welcome to Taprecruiter | Freelancer::";

        $Users = Users::where('user_type', $id)->where('status', 1)->get();
        
        // echo json_encode($Users);
        // exit;
        return response()->json(array(
                "error" => FALSE,
                "data" => $Users
        ));
    }


    public function search_freelancer(Request $request){

        $Country = $request->country;
        $State = $request->state;
        $City = $request->city;
        $Area = $request->area;
        $Gender = $request->gender;
        $UsertType = $request->user_type;

        // $request->session()->put('Country', $request->country);
        // $request->session()->put('State', $request->state);
        // $request->session()->put('City', $request->city);
        // $request->session()->put('Area', $request->area);
        // $request->session()->put('Gender', $request->gender);
        
        // echo Session::get('Country');
        // exit;

        $Users = Users::where('country', 'LIKE', "%{$Country}%")
                        ->Where('state', 'LIKE', "%{$State}%")
                        ->Where('city', 'LIKE', "%{$City}%")
                        ->Where('area', 'LIKE', "%{$Area}%")
                        ->Where('gender', 'LIKE', "%{$Gender}%")
                        ->Where('user_type', $UsertType)
                        ->where('status', 1)
                        ->get();


        return response()->json(array(
                "error" => FALSE,
                "data" => $Users
        ));
    }
}
