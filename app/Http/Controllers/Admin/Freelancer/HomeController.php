<?php

namespace App\Http\Controllers\Admin\Freelancer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard(){
        $title ="Dashboard";

        return view('Admin.freelancer.dashboard', compact('title'));
    }
}
