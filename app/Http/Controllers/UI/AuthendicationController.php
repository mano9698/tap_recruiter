<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthendicationController extends Controller
{
    public function admin_login(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckEmail = Users::where('email', $Email)->first();

        if($CheckEmail == null){

            return redirect()->back()->with('message','Please check your credentials');

        }else{

            if($CheckEmail->status == 0){

                return redirect()->back()->with('message','Your account is not activated. Please contact your administrator...');
                    
            }elseif($CheckEmail->user_type == 1){
                if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                    $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                    $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                    $request->session()->put('AdminProfilePic', Auth::guard('super_admin')->user()->profile_pic);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                    // echo "Success";
                }else{
                    return redirect()->back()->with('message','Please check your credentials');
                    
                }
            }elseif($CheckEmail->user_type == 2){
                if (Auth::guard('user')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('Username', Auth::guard('user')->user()->first_name);
                    $request->session()->put('UserEmail', Auth::guard('user')->user()->email);
                    $request->session()->put('UserId', Auth::guard('user')->user()->id);
                    $request->session()->put('UserProfilePic', Auth::guard('user')->user()->profile_pic);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/freelancer/dashboard');
                    // echo "Success";
                }else{
                    return redirect()->back()->with('message','Please check your credentials');
                    
                }
            }
        }
        
    }


    public function register(){
        $title ="::Welcome to Taprecruiter | Home::";

        return view('UI.layouts.register', compact('title'));
    }

    public function login(){
        
        $title ="::Welcome to Taprecruiter | Home::";

        return view('UI.layouts.login', compact('title'));
    }

    public function store_users(Request $request){
        

        $Users = new Users();

        $Users->first_name = $request->first_name;
        $Users->last_name = $request->last_name;
        $Users->email = $request->email;
        $Users->password = Hash::make($request->password);
        $Users->contact_no = $request->contact_no;
        $Users->gender = $request->gender;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;
        $Users->area = $request->area;
        $Users->status = 1;

        $Users->user_type = $request->user_type;

        // if($request->user_type = "2"){
        //     $Users->user_type = 2;
        // }
        // if($request->user_type = "3"){
        //     $Users->user_type = 3;
        // }
        // if($request->user_type = "4"){
        //     $Users->user_type = 4;
        //     // echo "Taprecruiter";
        // }
        // if($request->user_type = "5"){
        //     $Users->user_type = 5;
        // }
        
        // exit;
        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Registered Successfully');

    }
}
